const placeholder = document.getElementById("placeholder");
const cursor = document.getElementById("cursor");

// Code For the Typing Effect
const typing_speed = 50; // ms
const texts = [
  "Junior Web Developer",
  "UI/UX Designer",
];

var text_index = 0, added_text_index = 0, cursor_blinking = true;
let _text = texts[text_index];
var adding_text_end = false;

var placeholder_add = function()
{
  cursor_blinking = false;
  cursor.style.visibility = "visible";
  cursor_active = true;
  
  if (!adding_text_end)
  {
    placeholder.textContent += _text[added_text_index];
    
    added_text_index++;
    if (added_text_index < _text.length)
    {
      setTimeout(placeholder_add, typing_speed);
      return;
    }
  }
  
  text_index++;
  if (text_index === texts.length)
    text_index = 0;
  
  cursor_blinking = true;
  setTimeout(placeholder_next, 5000);
}

var placeholder_next = function()
{
  cursor_blinking = false;
  cursor.style.visibility = "visible";
  cursor_active = true;
  
  if (placeholder.innerText !== "")
  {
    placeholder.innerText = placeholder.innerText.slice(0, -1);
    setTimeout(placeholder_next, typing_speed);
    return;
  }
  _text = texts[text_index];
  added_text_index = 0;
  setTimeout(placeholder_add, typing_speed);
}

placeholder_next();

var cursor_active = true;
var cursor_handler = function()
{
  if (!cursor_blinking)
  {
    cursor.style.visibility = "visible";
    cursor_active = true;
    return;
  }
  
  if (cursor_active)
  {
    cursor.style.visibility = "hidden";
    cursor_active = false;
  }
  else
  {
    cursor.style.visibility = "visible";
    cursor_active = true;
  }
}

setInterval(cursor_handler, 500);


// Code For Navigation

// const allItems = document.querySelectorAll(".navMenu ul li a")

// allItems.forEach(item => {
//     item.addEventListener("click",function(e){
//         for(var i =0;i<allItems.length;i++){
//             allItems[i].classList.remove("active");
//         }
//         this.classList.add("active")
//     })
// })


const rightContainer = document.querySelector(".right-container");
const rightHalf = document.querySelector(".right-half");
const leftHalf = document.querySelector(".left-half");

const menu = document.querySelector('.menu');
const menuH = menu.offsetHeight;
// const vpH = verge.viewportH();
const bodyH = document.body.offsetHeight;

rightHalf.addEventListener('scroll',()=>{
  let windscroll = rightHalf.scrollTop || document.body.scrollTop;
  let height = rightHalf.scrollHeight - rightHalf.clientHeight;
  let scrolled = (windscroll / height) * 100;
  document.querySelector(".progress").style.width = scrolled + '%'
  // document.querySelector(".progress2").style.height = scrolled + '%'

  const distanceFromBottom = rightHalf.scrollHeight - (windscroll);
  const percentFromBottom = (distanceFromBottom / height);
  
  // if (windscroll <= 20) {
  //   menu.style.bottom = `calc(((100vh - 20px) - ${height}px) * 1`;
  // }
  
  // if (windscroll > 20 && distanceFromBottom > 50) {
  //   menu.style.bottom = `calc(((100vh - 20px) - ${height}px) * ${percentFromBottom})`;
  // }
  
  // if (distanceFromBottom <= 50) {
  //   menu.style.bottom = '20px';
  // }

  menu.style.top = 20 + scrolled/10 + '%'
})

const pos = document.documentElement;
pos.addEventListener('mousemove', e => {
  pos.style.setProperty('--x', e.clientX + 'px')
  pos.style.setProperty('--y', e.clientY + 'px')
})



// Navigation Bar



rightHalf.addEventListener('scroll', () => {
  
});


// Scroll Animatoin

document.addEventListener("DOMContentLoaded",function(){
  const navLinks = document.querySelectorAll('nav ul li a');
  console.log(navLinks)

  navLinks.forEach(link=>{
    link.addEventListener("click",function(e){
      e.preventDefault()
      const targetId = this.getAttribute("href").substring(1);
      const telement = document.getElementById(targetId)
      if(telement){
        telement.scrollIntoView({behavior: "smooth"})
        console.log(telement)
      }
    })
  })
})




